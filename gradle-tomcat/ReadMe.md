# environment requirement
 * java 11 
# run server
## run embedded tomcat9, rest api server in linux
 ```
  cd gradle-tomcat/
  ./gradlew  tomcatRun
 ``` 
## run embedded tomcat9, rest api server in windows
 ```
  cd gradle-tomcat/
  ./gradle.bat  tomcatRun 
 ```
# test api
## echo api
Server response Got! ? where ? is the request parameter xx's value 
```
curl http://localhost:8080/gradle-tomcat/rest/test?xx=xxx
```
Server response message if using this curl: 
```
Got! xx
```
## test whether is  Palidrome sting ?
Api response whether request parameter xx's value is a Palidrome.
Api return false if it is not a  Palidrome sting. Otherwise, ture.
``` 
curl http://localhost:8080/gradle-tomcat/rest/palindrome?xx=abcef
```
Example case response message 'false';
``` 
false
```

# reference
## jersey servlet config setting guild line
 * https://eclipse-ee4j.github.io/jersey.github.io/documentation/latest/modules-and-dependencies.html#d0e585
## jersey guildline
 * https://eclipse-ee4j.github.io/jersey.github.io/documentation/latest/getting-started.html
## tomcat 9 web.xml basic template from:
 * https://tomcat.apache.org/tomcat-9.0-doc/appdev/web.xml.txt


