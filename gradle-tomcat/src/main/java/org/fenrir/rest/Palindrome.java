package org.fenrir.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("palindrome")
public class Palindrome {


    /**
     * palindrome
     *
     * @param xxx
     * @return
     */
    @GET()
    @Produces(MediaType.TEXT_PLAIN)
    public boolean isPalindrome(@QueryParam(value = "xx") String xxx) {
        char[] chars = xxx.toCharArray();
        for (int i = 0, j = chars.length - 1; i <= j; i++, j--) {
            if (chars[i] != chars[j]) {
                return false;
            }
        }
        return true;
    }
}
