package org.fenrir.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("test")
public class Test {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET()
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(@QueryParam(value = "xx") String xxx) {
        return "Got! " + xxx;
    }
}